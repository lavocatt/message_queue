INCLUDE(FindPackageHandleStandardArgs)

FIND_PATH(EREBOR_WRAPPER_INCLUDE_DIR erebor_wrapper.h HINTS
    ${EREBOR_WRAPPER_PREFIX}/include/
  /usr/include/
  /usr/local/include/
  /opt/local/include/
  /sw/include/
)

FIND_LIBRARY(EREBOR_WRAPPER_LIBRARY NAMES erebor_wrapper HINTS
    ${EREBOR_WRAPPER_PREFIX}/lib
  /usr/lib
  /usr/local/lib
  /opt/local/lib
  /sw/lib
)

find_package_handle_standard_args(EREBOR_WRAPPER DEFAULT_MSG
    EREBOR_WRAPPER_INCLUDE_DIR
    EREBOR_WRAPPER_LIBRARY
)

if(EREBOR_WRAPPER_INCLUDE_DIR)
    SET(EREBOR_WRAPPER_FOUND 1 CACHE BOOL "Found EREBOR_WRAPPER libraries")
endif(EREBOR_WRAPPER_INCLUDE_DIR)

MARK_AS_ADVANCED(
    EREBOR_WRAPPER_INCLUDE_DIR
    EREBOR_WRAPPER_LIBRARY
    EREBOR_WRAPPER_FOUND
)


