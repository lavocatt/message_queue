#include "message_queue.h"

char* start_str = "{\"type\":\"START\",\"isQuestion\":true,\"isACK\":false,\"data\":\"\"}";
char* stop_str  = "{\"type\":\"STOP\",\"isQuestion\":true,\"isACK\":false,\"data\":\"\"}";
zmq_msg_t ACK;
char * ack_str;
char * nack_str;
zmq_msg_t NACK;
int* sockets_status;
int* sockets_volume;
int* sockets_ttsent;
int* sockets_nbmsg;
long long* sockets_accumulator_stuck;
long long* sockets_stuck_start;

int authorized_send = 0;

char connect_string[100];

long long current_timestamp() {
    struct timeval te; 
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}

void my_free (void *data, void *hint){
    free (data);
}

/*
 * add a zmq_msg_t in the input queue
 */
int hang_msg(zmq_msg_t* msg){
    hanger* my_hang = malloc(sizeof(hanger));
    my_hang->input_message = msg;
    my_hang->next = NULL;
    input_hanger_size += 1;
    if (input_hanger == NULL){
        input_hanger = my_hang;
    }else{
        hanger * this_hang = input_hanger;
        while(this_hang->next != NULL){
            this_hang = this_hang->next;
        }
        this_hang->next = my_hang;
    }
    return input_hanger_size;
}

/*
 * Return a zmq_msg_t if there is one in the queue. Do not remove the item from
 * the queue
 */
zmq_msg_t* gimme_first(){
    hanger* my_hang = input_hanger;
    if(my_hang == NULL){
        return NULL;
    }else{
        zmq_msg_t * msg = my_hang->input_message;
        return msg;
    }
}

/*
 * Return a zmq_msg_t if there is one in the queue. And remove it from the
 * queue.
 */
zmq_msg_t* pick_up_msg(){
    hanger* my_hang = input_hanger;
    if(my_hang == NULL){
        return NULL;
    }else{
        input_hanger = my_hang->next;
        zmq_msg_t * msg = my_hang->input_message;
        input_hanger_size -= 1;
        free(my_hang);
        return msg;
    }
}

/*
 * Retrieve a message from the producer
 */
void get_request_message(void** items, int nb_sending_sockets){
    // receive the message
    zmq_msg_t* msg = malloc(sizeof(zmq_msg_t));
    assert(zmq_msg_init(msg) == 0);
    if(zmq_msg_recv(msg, items[0], ZMQ_NOBLOCK) == -1){
        assert(errno == EAGAIN);
        free(msg);
    }else{
        last_second_received += zmq_msg_size(msg);
        hang_msg(msg);
    }
}

/*
 * forward a message to the consumer as a message part. close the message part
 * if the lenth sent reach max_effective_data_to_send_per_window
 */
int write_message_to_consumer(void** items, int i,
                                    int max_effective_data_to_send_per_window){
    int sent = 0;
    if(sockets_status[i] != SOCKET_READY_TO_SEND){
        return 0;
    }
    // take an item, create a new message, zero copy from input
    // message to the output message and send it over the network
    // only wait for pollin when enough data are sent.
    zmq_msg_t * input_message = gimme_first();
    //if there is a message to handle
    //and if there is a token to send a message
    if(authorized_send > 0 && input_message != NULL){
        zmq_msg_t output_message;//TODO maybe a bug here, need to malloc
        zmq_msg_init(&output_message);
        assert(zmq_msg_move(&output_message, input_message) !=-1);
        int v = zmq_msg_size(&output_message);
        sockets_volume[i] += v;
        /**
         * once this socket has sent enough data, it will need to send
         * the last message part and change the socket status
         */
        if(sockets_volume[i] >= max_effective_data_to_send_per_window){
            if(zmq_msg_send(&output_message, items[1+(i*2)], ZMQ_NOBLOCK) == -1){
                assert(errno == EAGAIN);
            }else{
                // set the flag to waiting ack
                sockets_status[i] = SOCKET_WAITING_ACK;
                sockets_stuck_start[i] = current_timestamp();
                sent++;
            }
        }else{
            if(zmq_msg_send(&output_message, items[1+(i*2)],ZMQ_NOBLOCK|ZMQ_SNDMORE) == -1){
                assert(errno == EAGAIN);
            }else{
                sent++;
            }
        }
        if(sent){
            pick_up_msg();
            sockets_nbmsg[i]++;
            assert(zmq_msg_close(input_message) != -1);
            assert(zmq_msg_close(&output_message) != -1);
            free(input_message);
            // if the full message is sent, consume the token
            if(sockets_status[i] == SOCKET_WAITING_ACK){
                authorized_send -=1;
            }
        }else{
            sockets_volume[i] -= v;
            assert(zmq_msg_move(input_message, &output_message) !=-1);
        }
    }
    return sent;
}

/*
 * Finalize the sending. If the socket was sending parts, send a zero length
 * message. Then send a 2byte message to the consumer indicating that the work
 * is done.
 */
void end_connection(void** items, int i,
                                    int max_effective_data_to_send_per_window){
    zmq_msg_t output_message;
    assert(zmq_msg_init_size(&output_message, 0) != -1);
    if(sockets_volume[i] > 0){
        assert(zmq_msg_send(&output_message, items[1+(i*2)], 0) != -1);
    }
    assert(zmq_msg_close(&output_message) != -1);
    zmq_msg_t output_message2;
    char* end = malloc(2);
    assert(zmq_msg_init_data(&output_message2, end, 2, my_free, NULL) != -1);
    if(sockets_volume[i] > 0){
        assert(zmq_msg_send(&output_message2, items[1+(i*2)], 0) != -1);
    }
    assert(zmq_msg_close(&output_message2) != -1);
}

/*
 * Acquire the ack from receiver and update the socket state
 */
void get_ack_from_consumer(void** items, int i){
    zmq_msg_t msg;
    assert(zmq_msg_init(&msg) == 0);
    if(zmq_msg_recv(&msg, items[2+(i*2)], ZMQ_NOBLOCK) == -1){
        assert(errno == EAGAIN);
    }else{
        void*  data      = zmq_msg_data(&msg);
        size_t data_size = zmq_msg_size(&msg);
        char*  rebuilt_string = malloc(data_size+1);
        memcpy(rebuilt_string, data, data_size);
        rebuilt_string[data_size] = 0x00;
        // Check if it's a nack or an ack
        if(strncmp(rebuilt_string, ack_str, 3) == 0){
            // success receiving data
            last_second_sent += sockets_volume[i];
            sockets_ttsent[i]+= sockets_volume[i];
            sockets_status[i] = SOCKET_READY_TO_SEND;
            sockets_volume[i] = 0;
            sockets_accumulator_stuck[i]+=current_timestamp() - sockets_stuck_start[i];
        }else{
            printf("TODO received a nack\n");
        }
        free(rebuilt_string);
        assert(zmq_msg_close(&msg) != -1);
    }
}

/*
 * erebor* e                : initialized erebor structure, pointer not managed
 *                            here
 * char* username           : System user name
 * int   tempon             : size of the output buffer
 * int   duration           : wall running time
 * int   sending_port       : Port to be used to send data to the consumer
 * int   receiving_port     : Port to be used to receive data from the producer
 * int   nb_sending_sockets : How many output sockets
 */
void do_queue(erebor* e, char* username,
        int max_effective_data_to_send_per_window, int duration,
        int sending_port, int receiving_port, int nb_sending_sockets, int rank){

    // ini csv files
    float second = 0;
    char* output_csv = malloc(100);
    sprintf(output_csv, "/home/%s/Documents/these/output%d.csv", username, rank);
    FILE* outcsv = fopen(output_csv, "w");
    fprintf(outcsv, "seconds,how,type\n");
    char* output_csv2 = malloc(100);
    sprintf(output_csv2, "/home/%s/Documents/these/lags%d.csv", username, rank);
    FILE* outcsv2 = fopen(output_csv2, "w");
    fprintf(outcsv2, "seconds,how,type\n");

    // init generic messages
    ack_str = malloc(4);
    memcpy(ack_str, "ACK", 3);
    ack_str[3] = 0x00;
    assert(zmq_msg_init_data(&ACK, ack_str, 4, my_free, NULL) != -1);
    nack_str = malloc(5);
    memcpy(nack_str, "NACK", 5);
    nack_str[4] = 0x00;
    assert(zmq_msg_init_data(&ACK, nack_str, 5, my_free, NULL) !=-1);

    // init status for sockets
    sockets_status = malloc(nb_sending_sockets*sizeof(int));
    sockets_volume = malloc(nb_sending_sockets*sizeof(int));
    sockets_ttsent = malloc(nb_sending_sockets*sizeof(int));
    sockets_nbmsg  = malloc(nb_sending_sockets*sizeof(int));
    sockets_accumulator_stuck  = malloc(nb_sending_sockets*sizeof(long long));
    sockets_stuck_start        = malloc(nb_sending_sockets*sizeof(long long));
    memset(sockets_status, 0, nb_sending_sockets*sizeof(int));
    memset(sockets_volume, 0, nb_sending_sockets*sizeof(int));
    memset(sockets_ttsent, 0, nb_sending_sockets*sizeof(int));
    memset(sockets_nbmsg,  0, nb_sending_sockets*sizeof(int));
    memset(sockets_accumulator_stuck,  0, nb_sending_sockets*sizeof(long long));
    memset(sockets_stuck_start,        0, nb_sending_sockets*sizeof(long long));
    for(int i=0; i<nb_sending_sockets; i++){
        sockets_status[i] = SOCKET_READY_TO_SEND;
    }

    // initialise zmq
    void ** sockets  = malloc(sizeof(void**)*(1+(nb_sending_sockets*2)));
    void * context = zmq_ctx_new();
    zmq_ctx_set(context, ZMQ_IO_THREADS, 20);
    void * input_socket = zmq_socket(context, ZMQ_PULL); // producer
    sprintf (connect_string, "tcp://*:%d", receiving_port);
    if (zmq_bind (input_socket, connect_string) == -1){
        erebor_print_error();
        printf("failure binding REP %s", connect_string);
        fflush(stdout);
        exit(-1);
    }
    printf("REP connected %s\n", connect_string);
    sockets[0] = input_socket;
    for(int i=0; i<nb_sending_sockets; i++){ // for the consumer
        void* pusher = zmq_socket(context, ZMQ_PUSH);
        void* puller = zmq_socket(context, ZMQ_PULL);
        sockets[1+(i*2)] = pusher;
        sockets[2+(i*2)] = puller;
        //bind the sockets
        sprintf (connect_string, "tcp://*:%d", sending_port);
        if (zmq_bind (pusher, connect_string) == -1){
            printf("failure connecting puller %d %s", i, connect_string);
            erebor_print_error();
            fflush(stdout);
            exit(-1);
        }
        printf("pusher bound %s\n", connect_string);
        sprintf (connect_string, "tcp://*:%d", sending_port+1);
        if(zmq_bind (puller, connect_string) == -1){
            printf("failure connecting pusher %d %s", i, connect_string);
            erebor_print_error();
            fflush(stdout);
            exit(-1);
        }
        printf("puller bound %s\n", connect_string);
        sending_port+=2;
    }


    // Time tracking to work only for Duration
    int keepup = 1;
    long long start_time     = current_timestamp();
    long long second_tracker = start_time;
    /**************************************************************************
     *
     *                          Main Event Loop
     *
     *************************************************************************/
    /*
     * get message from queue
     */
    int* free_to_send = malloc(nb_sending_sockets*4);
    int count = 0;
    int last_start = 0;
    long long whithout_write_start = current_timestamp();
    long long total_without_write = 0;
    while(keepup){
        /*
         * get message from queue
         */
        get_request_message(sockets, nb_sending_sockets);
        /**
         * Process which output socket is ready to send and try to see if an ack
         * has been received for the other ones.
         */
        for(int i=0; i<nb_sending_sockets;i++){
            get_ack_from_consumer(sockets, i);
        }
        if(input_hanger_size > 0){
            for(int i=last_start; i<nb_sending_sockets+last_start;i++){
                int k = (i % nb_sending_sockets);
                if(sockets_status[k] == SOCKET_READY_TO_SEND){
                    int ret = write_message_to_consumer(sockets, k, max_effective_data_to_send_per_window);
                    if (ret > 0){
                        long long now = current_timestamp();
                        total_without_write += now - whithout_write_start;
                        whithout_write_start = now;
                    }
                }
            }
            last_start++;
            if(last_start == nb_sending_sockets){
                last_start = 0;
            }
        }
        if(count % 100 == 0){
            count = 0;
            long long now = current_timestamp();
            keepup = now - start_time < duration;
            // Every seconds write CSV
            if(!keepup || now - second_tracker > ONE_SECOND){
                second += ((now -second_tracker) / ONE_SECOND);
                second_tracker = now;
                fprintf(outcsv, "%f,%lld,%s%d\n", second, (long
                            long)last_second_received, "input", rank);
                last_second_received = 0;
                for(int i=0; i<nb_sending_sockets;i++){
                    fprintf(outcsv, "%f,%d,%s%i\n", second, sockets_ttsent[i],
                            "socket", i+((rank-1)*nb_sending_sockets));
                    sockets_ttsent[i] = 0;
                    fprintf(outcsv2, "%f,%lld,%s%i\n", second, sockets_accumulator_stuck[i],
                            "socket", i+((rank-1)*nb_sending_sockets));
                    //sockets_accumulator_stuck[i] = 0;
                }
                //Give tokens to send message for slow start
                authorized_send = ((int)second) * nb_sending_sockets;
            }
        }
        count++;
    }
    fprintf(outcsv2, "%f,%lld,global\n", second, total_without_write);
    printf("Total time without write %lld ms\n", total_without_write);
    if(input_hanger_size > 0){
        printf("Still have messages to send %d\n", input_hanger_size);
    }else{
        printf("Every messages sent\n");
    }

    for(int i=0; i<nb_sending_sockets; i++){
        end_connection (sockets, i, max_effective_data_to_send_per_window);
    }
    free(sockets_status);
    free(sockets_volume);
    free(sockets_ttsent);
    free(sockets_nbmsg);
    free(ack_str);
    free(nack_str);
    free(free_to_send);
    // close sockets and context
    printf("closing sockets\n");
    for(int i=0; i<1+(nb_sending_sockets*2); i++){
        zmq_close(sockets[i]);
        //printf("socket %d closed\n", i);
        //fflush(stdout);
    }
    fflush(outcsv);
    fflush(outcsv2);
    fclose(outcsv);
    fclose(outcsv2);
    printf("terminateing context\n");
    fflush(stdout);
    zmq_ctx_term(context);
    printf("destroying context context\n");
    fflush(stdout);
    zmq_ctx_destroy(context);
    printf("Can die\n");
}

int main(int argc, char** argv){
    //Parse options
    int opt;
    char* in_ipc;
    char* out_ipc = malloc(100);
    char* network;
    char* server;
    char*  username;
    int rank,tempon, duration, sending_port, receiving_port, nb_sending_sockets;
    do{
        opt = getopt (argc, argv, "R:S:P:N:U:T:D:B:H:");
        switch (opt) {
            case 'R':
                rank = atoi(optarg);
                break;
            case 'S':
                server = optarg;
                break;
            case 'P':
                in_ipc = optarg;
                int iout_ipc = atoi(optarg) + 300 + rank;
                sprintf(out_ipc, "%d", iout_ipc);
                break;
            case 'N':
                network = optarg;
                break;
            case 'U':
                username = optarg;
                break;
            case 'T':
                tempon = atoi(optarg);
                break;
            case 'D':
                duration = atoi(optarg);
                break;
            case 'B':
                sending_port   = atoi(optarg);
                receiving_port = sending_port -1111;
                break;
            case 'H':
                nb_sending_sockets = atoi(optarg);
                break;
        }
    } while (opt != -1);
    // Variables to handle answers
    char *dest    = malloc(100);
    char *group   = malloc(100);
    char *message = malloc(1000);

    // Connect to Yggdrasil
    char* str_rank = malloc(100);
    sprintf(str_rank, "%d", rank);
    printf("client %d %s %s %s %s\n", rank, in_ipc, out_ipc, network, server);
    erebor* e = malloc(sizeof(erebor));
    int ret = erebor_init_connection(str_rank, network, in_ipc, out_ipc, e);

    if(ret == -1){
        erebor_print_error();
    }else{
        // Notify  Spark that we start
        erebor_send_to(e, "0", server, start_str);
        int keepup = 1;
        int keep_wait = 1;
        long long start_time = current_timestamp();
        while(keepup && keep_wait){
            keep_wait = erebor_non_block_recv(e, dest, group, message); //drop the answer
            assert(keep_wait != -1);
            long long now = current_timestamp();
            keepup = now - start_time < duration;
        }
        if(keep_wait != 0){
            printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            printf("!!                                                                            !!\n");
            printf("!!                                                                            !!\n");
            printf("!!                                                                            !!\n");
            printf("!!                     Failed at first contact                                !!\n");
            printf("!!                                                                            !!\n");
            printf("!!                                                                            !!\n");
            printf("!!                                                                            !!\n");
            printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        }else{
            //do proper work
            do_queue(e, username, tempon, duration, sending_port, receiving_port,
                        nb_sending_sockets, rank);
            // Notify  Spark that we stop
            erebor_send_to(e, "0", server, stop_str);
            // Wait for synchrony before stopping the engine.
            keepup  = 1;
            keep_wait = 1;
            start_time = current_timestamp();
            while(keepup && keep_wait){
                int ret = erebor_non_block_recv(e, dest, group, message); //drop the answer
                assert(ret != -1);
                keep_wait = ret == -2;
                long long now = current_timestamp();
                keepup = now - start_time < 4000;
            }
            if(keep_wait == 1){
                printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                printf("!!                                                                            !!\n");
                printf("!!                                                                            !!\n");
                printf("!!                                                                            !!\n");
                printf("!!                            ERROR                                           !!\n");
                printf("!!                                                                            !!\n");
                printf("!!                                                                            !!\n");
                printf("!!                                                                            !!\n");
                printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            }
        }
        // Close Yggdrasil connexion
        erebor_close(e);
    }
    free(dest);
    free(group);
    free(message);
    free(str_rank);
    free(e);
    return 0;
}
