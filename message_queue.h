#ifndef MQ
#define MQ

#include <math.h>
#include <stdio.h>
#include <erebor_wrapper.h>
#include <zmq.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>
#include <sys/time.h>
#include <assert.h>

#define MAX_INPUT_QUEU       1740000000 //max 1gig input queue
#define ONE_SECOND           1000
#define SOCKET_READY_TO_SEND 2
#define SOCKET_WAITING_ACK   4

typedef struct item{
    zmq_msg_t * input_message;
    struct item* next;
} hanger;

int      nb_input_messages_waiting_to_be_polled = 0;
int      need_output_socket   = 0;
int      input_hanger_size    = 0;
hanger*  input_hanger         = NULL;
uint64_t last_second_received = 0;
uint64_t last_second_sent     = 0;

#endif

